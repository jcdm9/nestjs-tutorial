## Description

NestJS Tutorial

## Pre-requisite

```bash
Docker
yarn
```

## Installation

```bash
$ yarn
```

## Setup database

```bash
# On root directory, run this command
$ docker-compose up db -d
```

## Running the app

```bash
# watch mode
$ npm run start:dev
```

## environment file

```bash
# for testing purposes here's the .env content
API_KEY=my-api-key
DB_HOST=localhost
DB_PORT=5432
DB_USERNAME=postgres
DB_PASSWORD=pass123
DB_NAME=postgres
```

## API Document

```bash
Once application is running, go to http://localhost:3000/api
```
